#!/bin/bash

set -eu

pkgname=iceape
brandingver=2.39
brandingrel=1

srcdir=src
distdir=${pkgname}-${brandingver}
tarball=${pkgname}_${brandingver}-${brandingrel}.branding.tar.xz

rm -rf $distdir/
mkdir -pv $distdir

cp -rv $srcdir/* $distdir/

rm -f ${tarball}{,.sig}
tar -cJf $tarball $distdir
gpg --output ${tarball}.sig --detach-sig $tarball
rm -rf $distdir

echo "dist tarball:   $tarball"
echo "dist signature: $tarball.sig"
